// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Animation/AnimInstance.h"
#include "Character3DAnimInstance.generated.h"

UENUM(BlueprintType, Blueprintable)
enum class EFoot : uint8
{
	LEFT	= 0 UMETA(DisplayName = "LeftFoot"),
	RIGHT	= 1 UMETA(DisplayName = "RightFoot"),
	BOTH	= 2 UMETA(DisplayName = "BothFoot")
};


DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FOnFootStep, EFoot, Foot, float, Strength);

DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnMeleeAttackFinished);
DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnMeleeAttackStart);
/**
 * 
 */
UCLASS()
class AIBOTS_API UCharacter3DAnimInstance : public UAnimInstance
{
	GENERATED_BODY()
public:
	//~ Begin Orientation vectors in Component Space
	UPROPERTY(EditDefaultsOnly,BlueprintReadOnly)
	FVector AnimForwardVector{0, 1, 0};
	
	UPROPERTY(EditDefaultsOnly,BlueprintReadOnly)
	FVector AnimBackwardVector{0,-1,0};
	
	UPROPERTY(EditDefaultsOnly,BlueprintReadOnly)
	FVector AnimLeftVector{1,0,0};
	
	UPROPERTY(EditDefaultsOnly,BlueprintReadOnly)
	FVector AnimRightVector{-1,0,0};
	
	UPROPERTY(EditDefaultsOnly,BlueprintReadOnly)
	FVector AnimUpVector{0,0,1};
	
	UPROPERTY(EditDefaultsOnly,BlueprintReadOnly)
	FVector AnimDownVector{0,0,-1};
	//~ End Orientation vectors in Component Space
	
public:

	virtual void BeginDestroy() override;
	
	/** Rotate weapon hand in Component Space
	 * @param Rotator Rotator in Component Space
	 */
	UFUNCTION(BlueprintImplementableEvent)
	void RotateWeaponHand(FRotator Rotator);

	/** Rotate upper body in Forward Space
	 * @param Rotator Rotator in Component Space
	 */
	UFUNCTION(BlueprintImplementableEvent)
	void RotateObserver(FRotator Rotator);

	/** Event about foot step
	 * 
	 */
	UPROPERTY(BlueprintAssignable, BlueprintCallable)
	FOnFootStep OnFootStep;

	//Do  MeleeAttack
	UFUNCTION(BlueprintImplementableEvent,BlueprintCallable)
    void DoMeleeAttack();

	/**On melee attack finished
	 */
	UPROPERTY(BlueprintAssignable, BlueprintCallable)
	FOnMeleeAttackFinished OnMeleeAttackFinished;

	/**On melee atack start
	 */
	UPROPERTY(BlueprintAssignable,BlueprintCallable)
	FOnMeleeAttackStart OnMeleeAttackStart;
	
};
