// Fill out your copyright notice in the Description page of Project Settings.


#include "Character3DAnimInstance.h"

void UCharacter3DAnimInstance::BeginDestroy()
{
	
	//Clear any active timer of actor
	auto World = GetWorld();
	if(World)
		World->GetTimerManager().ClearAllTimersForObject(this);

	Super::BeginDestroy();
}
