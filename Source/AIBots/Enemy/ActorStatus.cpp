// Fill out your copyright notice in the Description page of Project Settings.


#include "ActorStatus.h"
#include "GameFramework/Actor.h"

// Sets default values for this component's properties
UActorStatus::UActorStatus()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	//InitHealth
	Health = MaxHealth;
}

bool UActorStatus::IsDead()
{
	return HealthStatus == EEnumActorHealthStatus::DEAD;
}


// Called when the game starts
void UActorStatus::BeginPlay()
{
	Super::BeginPlay();

	// Set initial  health and send notification
	ChangeHealth(0);

	//Bind event OnTakeAnyDamage
	AActor* Owner = GetOwner();
	if(IsValid(Owner))
	{
		Owner->OnTakeAnyDamage.AddDynamic(this, &UActorStatus::OnTakeAnyDamage);
	}
}


void UActorStatus::RefreshHealthStatus()
{
	if (Immortal)
	{
		SetHealthStatus(EEnumActorHealthStatus::FULL_HEALTH);
	}
	else if (Health >= MaxHealth)
	{
		SetHealthStatus( EEnumActorHealthStatus::FULL_HEALTH );
	}
	else if (Health > 0)
	{
		SetHealthStatus( EEnumActorHealthStatus::INJURED );
	}
	else
	{
		if (GetHealthStatus()!= EEnumActorHealthStatus::DEAD)
		{
			AActor* Owner = GetOwner();
			if (IsValid(Owner))
			{
				OnCharacterDeadEvent.Broadcast(Owner);
			}
			SetHealthStatus( EEnumActorHealthStatus::DEAD );
		}
	}
}



void UActorStatus::OnTakeAnyDamage(AActor* DamagedActor, float Damage, const UDamageType* DamageType,
                                   AController* InstigatedBy, AActor* DamageCauser)
{
	ChangeHealth(-Damage);
}


float UActorStatus::ChangeHealth(float delta)
{
	auto actualDelta = 0.f;
	if (!Immortal)
	{
		auto before = Health;
		Health = FMath::Min(FMath::Max(Health + delta, 0.f), MaxHealth);
		actualDelta = Health - before;
	}
	RefreshHealthStatus();
	OnHealthChangeEvent.Broadcast(this);

	return actualDelta;
}
