// Fill out your copyright notice in the Description page of Project Settings.


#include "EnemyCharacter.h"
#include "Kismet/KismetMathLibrary.h"
#include "Animation/Character3DAnimInstance.h"
#include "Components/CapsuleComponent.h"

// Sets default values
AEnemyCharacter::AEnemyCharacter(const FObjectInitializer& ObjectInitializer) :
Super(ObjectInitializer)
{
 	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	//Create ActorStatus
	ActorStatus = CreateDefaultSubobject<UActorStatus>(TEXT("ActorStatus"));
	if(ActorStatus)
	{
		ActorStatus->SetActive(true);
	}

	//Init Aiming and Tracking variables
	Aiming = false;
	AimPointDefault = {0,
		10 * GetCapsuleComponent()->GetScaledCapsuleRadius(),
		GetCapsuleComponent()->GetScaledCapsuleHalfHeight()};
	Tracking = false;
	TrackingPointDefault = {0,
        10 * GetCapsuleComponent()->GetScaledCapsuleRadius(),
        2 * GetCapsuleComponent()->GetScaledCapsuleHalfHeight()};

	//Initialize default main and second class
	DefaultMainWeaponClass = AEnemyWeapon::StaticClass();
	DefaultSecondWeaponClass = AEnemyWeapon::StaticClass();
}

void AEnemyCharacter::EndPlay(const EEndPlayReason::Type EndPlayReason)
{
	

	//Clear any active timer of actor
	auto World = GetWorld();
	if(World)
		World->GetTimerManager().ClearAllTimersForObject(this);

	Super::EndPlay(EndPlayReason);
}

bool AEnemyCharacter::IsDead()
{
	return ActorStatus->GetHealthStatus()==EEnumActorHealthStatus::DEAD;
}


// Called when the game starts or when spawned
void AEnemyCharacter::BeginPlay()
{
	Super::BeginPlay();

	//Bind events OnHealthChange OnCharacterDead
	if (IsValid(ActorStatus))
	{
		ActorStatus->OnHealthChangeEvent.AddDynamic(this, &AEnemyCharacter::OnHealthChange);
		ActorStatus->OnCharacterDeadEvent.AddDynamic(this, &AEnemyCharacter::OnCharacterDead);
	}

	//Bind event OnCharacterMovementUpdated
	OnCharacterMovementUpdated.AddDynamic(this, &AEnemyCharacter::OnCharacterMovementUpdatedFunction);

	//Bind OnMeleeAttackFinished
	auto CharacterMesh = GetMesh();
	if (IsValid(CharacterMesh))
	{
		auto AnimInst = Cast<UCharacter3DAnimInstance>(CharacterMesh->GetAnimInstance());
		if(AnimInst)
			AnimInst->OnMeleeAttackFinished.AddDynamic(this,&AEnemyCharacter::OnMeleeAttackFinished_Inner);
	}
}

void AEnemyCharacter::BeginDestroy()
{
	Super :: BeginDestroy();
}

void AEnemyCharacter::OnCharacterDead_Implementation(AActor* Actor)
{
	//Detach and Destroy Controller
	DetachFromControllerPendingDestroy();

	//Send event OnCharacterDeadEvent
	OnCharacterDeadEvent.Broadcast(this);
	
	//Destruct Actor after DestructTime
	if (DestructTime == 0)
	{
		InnerDestroy();
	}
	else if (DestructTime>0)
	{
		FTimerHandle TimerHandle;
		GetWorldTimerManager().SetTimer(TimerHandle, this, &AEnemyCharacter::InnerDestroy, DestructTime);
	}
}

float AEnemyCharacter::InternalTakePointDamage(float Damage, FPointDamageEvent const& PointDamageEvent,
	AController* EventInstigator, AActor* DamageCauser)
{
	if (!HitScoreTableClass) return Super::InternalTakePointDamage(Damage, PointDamageEvent, EventInstigator, DamageCauser);

	FName BoneName = PointDamageEvent.HitInfo.BoneName;
	float Multiplyer = HitScoreTableClass->GetDefaultObject<UHitScoreTable>()->GetScore(BoneName.ToString());

	return Damage * Multiplyer;
}

// Called every frame
void AEnemyCharacter::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	
	if (Aiming)	AimToPoint(AimPoint);
	if (Tracking) TrackPoint(TrackingPoint);
}

// Called to bind functionality to input
void AEnemyCharacter::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

}

bool AEnemyCharacter::AimToPointInner(FVector NewAimPoint)
{
	auto CharacterMesh = GetMesh();
	if (IsValid(CharacterMesh))
	{
		UCharacter3DAnimInstance* AnimInstance = Cast<UCharacter3DAnimInstance>(CharacterMesh->GetAnimInstance());
		if (IsValid(AnimInstance))
		{
				//Calculate Relative Aim Point in animation component space
				FVector RelativeAimPoint;
				FRotator RelativeRotator;
				
				CharacterMesh->TransformToBoneSpace(TEXT("root"),
                    NewAimPoint,
                    FRotator::ZeroRotator,
                    RelativeAimPoint,
                    RelativeRotator
                    );
				
				//Remember Aim Point
				AimPoint = NewAimPoint;
	
				return AimToPointRelative(RelativeAimPoint);
		}
	}
	return false;
}

bool AEnemyCharacter::TrackPointRelative(FVector NewTrackingPoint)
{
	
	auto CharacterMesh = GetMesh();
	if (IsValid(CharacterMesh))
	{
		UCharacter3DAnimInstance* AnimInstance = Cast<UCharacter3DAnimInstance>(CharacterMesh->GetAnimInstance());
		if (IsValid(AnimInstance))
		{
			//Calculate Relative rotator in animation forward space
			FRotator RelativeRotator;
			
			//Get tracking point relative Head height
			FVector TrackingPointFromHead = NewTrackingPoint;
			TrackingPointFromHead.Z -= 2 * GetCapsuleComponent()->GetScaledCapsuleHalfHeight();
			
			RelativeRotator = TrackingPointFromHead.Rotation();
			
			FRotator RotatorInForwardSpace = UKismetMathLibrary::ComposeRotators(
                RelativeRotator,
                UKismetMathLibrary::NegateRotator(AnimInstance->AnimForwardVector.Rotation()));

			//Call AnimInstance function for Rotation control 
			AnimInstance->RotateObserver(RotatorInForwardSpace);
			
			return true;
		}
	}
	return false;
}

bool AEnemyCharacter::AimToPointRelative(FVector NewAimPoint)
{
	
	auto CharacterMesh = GetMesh();
	if (IsValid(CharacterMesh))
	{
		UCharacter3DAnimInstance* AnimInstance = Cast<UCharacter3DAnimInstance>(CharacterMesh->GetAnimInstance());
		if (IsValid(AnimInstance))
		{
			FVector MuzzlePoint;
			if(GetMuzzleLocation(MuzzlePoint))
			{
				//Calculate Relative rotator in animation component space
				FVector RelativeMuzzlePoint;
				FRotator RelativeRotator;
				 CharacterMesh->TransformToBoneSpace(TEXT("root"),
                    MuzzlePoint,
                    FRotator::ZeroRotator,
                    RelativeMuzzlePoint,
                    RelativeRotator
                    );

				FRotator RotatorInComponentSpace = (RelativeMuzzlePoint-NewAimPoint).Rotation();
				
				//Call AnimInstance function for Rotation control
				AnimInstance->RotateWeaponHand(RotatorInComponentSpace);
	
				return true;
			}
		}
	}
	return false;
}

void AEnemyCharacter::OnMeleeAttackFinished_Inner()
{
	OnMeleeAttackFinished.Broadcast();
}

void AEnemyCharacter::InnerDestroy()
{
	//Clear any active timer of actor
	auto World = GetWorld();
	if(World)
		World->GetTimerManager().ClearAllTimersForObject(this);
	if (IsValid(MainWeapon))
		MainWeapon->Destroy();
	if (IsValid(SecondWeapon))
		SecondWeapon->Destroy();
	Destroy();
}

bool AEnemyCharacter::AimToPoint(FVector NewAimPoint)
{
	Aiming = true;
	return AimToPointInner(NewAimPoint);
}

void AEnemyCharacter::AimPointReset()
{
	Aiming = false;
	AimToPointRelative(AimPointDefault);
}

bool AEnemyCharacter::TrackPoint(FVector NewTrackingPoint)
{
	Tracking = true;
	return TrackPointInner(NewTrackingPoint);
}

bool AEnemyCharacter::TrackPointInner(FVector NewTrackingPoint)
{
	auto CharacterMesh = GetMesh();
	if (IsValid(CharacterMesh))
	{
		UCharacter3DAnimInstance* AnimInstance = Cast<UCharacter3DAnimInstance>(CharacterMesh->GetAnimInstance());
		if (IsValid(AnimInstance))
		{
			//Calculate Relative rotator in animation forward space
			FVector RelativeTrackingPoint;
			FRotator RelativeRotator;
			CharacterMesh->TransformToBoneSpace(TEXT("root"),
				NewTrackingPoint,
				FRotator::ZeroRotator,
				RelativeTrackingPoint,
				RelativeRotator);

			//Remember Tracking Point
			TrackingPoint = NewTrackingPoint;
			
			return TrackPointRelative(RelativeTrackingPoint);
        }
	}
	return false;
}

void AEnemyCharacter::TrackingPointReset()
{
	Tracking = false;
	TrackPointRelative(TrackingPointDefault);
}

void AEnemyCharacter::Shoot_Implementation()
{
}

bool AEnemyCharacter::ShootThePoint(FVector Point)
{
	//Aim at the point
		AimToPoint(Point);
		TrackPoint(Point);
	//Shoot
	    Shoot();
	
	return true;
}

void AEnemyCharacter::DoMeleeAttack_Implementation()
{
}

AEnemyWeapon* AEnemyCharacter::SetMainWeapon(AEnemyWeapon* NewWeapon, TSubclassOf<AEnemyWeapon> EnemyWeaponClass, bool DestroyOldWeapon)
{
	DropMainWeapon(DestroyOldWeapon);
	
	if (IsValid(NewWeapon))
	{
		//Set new MainWeapon from existing one

		NewWeapon->SetOwner(this);
		NewWeapon->SetInstigator(this);
		NewWeapon->OnPickup();
		MainWeapon = NewWeapon;
		MainWeapon->AttachToComponent(GetMesh(),FAttachmentTransformRules::SnapToTargetIncludingScale, WeaponActive_SocketName );
		
		if (WeaponController) WeaponController->SetWeapon(MainWeapon);
	}
	else
	{
		//Set new MainWeapon from class
		
		if (EnemyWeaponClass == nullptr)
			EnemyWeaponClass = DefaultMainWeaponClass;
		
		if (GetWorld())
			MainWeapon = GetWorld()->SpawnActorDeferred<AEnemyWeapon>(EnemyWeaponClass,FTransform::Identity, this, nullptr, ESpawnActorCollisionHandlingMethod::AlwaysSpawn);
		
		if(IsValid(MainWeapon))
		{

			MainWeapon->FinishSpawning(FTransform::Identity);
			MainWeapon->OnPickup();
			MainWeapon->AttachToComponent(GetMesh(),FAttachmentTransformRules::SnapToTargetIncludingScale, WeaponActive_SocketName );
			
			if (WeaponController) WeaponController->SetWeapon(MainWeapon);
		}
	}

	//Send Notification OnMainWeaponChanged with proper value 
	if(IsValid(MainWeapon))
	{
		OnMainWeaponChanged.Broadcast(MainWeapon);
		return MainWeapon;
	}
	else
	{
		OnMainWeaponChanged.Broadcast(MainWeapon);
		return nullptr;
	}
}

AEnemyWeapon* AEnemyCharacter::SetSecondWeapon(AEnemyWeapon* NewWeapon, TSubclassOf<AEnemyWeapon> EnemyWeaponClass, bool DestroyOldWeapon)
{
	DropSecondWeapon(DestroyOldWeapon);

	if(IsValid(NewWeapon))
	{
		//Set SecondWeapon from existing one
		NewWeapon->SetOwner(this);
		NewWeapon->SetInstigator(this);
		NewWeapon->OnPickup();
		SecondWeapon = NewWeapon;
		SecondWeapon->AttachToComponent(GetMesh(),FAttachmentTransformRules::SnapToTargetIncludingScale, WeaponStack_SocketName );
		return SecondWeapon;
	}
	else
	{
		//Set SecondWeapon from class
		
		if (EnemyWeaponClass == nullptr)
			EnemyWeaponClass = DefaultSecondWeaponClass;

		if (GetWorld())
			SecondWeapon = GetWorld()->SpawnActorDeferred<AEnemyWeapon>(EnemyWeaponClass,FTransform::Identity, this, nullptr, ESpawnActorCollisionHandlingMethod::AlwaysSpawn);
	
		if(IsValid(SecondWeapon))
		{
			SecondWeapon->FinishSpawning(FTransform::Identity);
			SecondWeapon->OnPickup();
			SecondWeapon->AttachToComponent(GetMesh(),FAttachmentTransformRules::SnapToTargetIncludingScale, WeaponStack_SocketName );
			return SecondWeapon;
		}
	}
	return nullptr;
}

AEnemyWeapon* AEnemyCharacter::DropMainWeapon(bool DestroyWeapon)
{
	AEnemyWeapon* RetVal = nullptr;
	if (WeaponController) WeaponController->SetWeapon(nullptr);

	if (MainWeapon!=nullptr)
	{
		if (DestroyWeapon)
		{		
			MainWeapon->Destroy();
			MainWeapon = nullptr;
		}
		else 
		{
			if (IsValid(MainWeapon))
			{
				MainWeapon->DetachFromActor(FDetachmentTransformRules::KeepWorldTransform);
				MainWeapon->SetOwner(nullptr);
				MainWeapon->SetInstigator(nullptr);
				MainWeapon->OnDrop();
			
				RetVal = MainWeapon;
			}
		
			MainWeapon = nullptr;
		}
	}

	OnMainWeaponChanged.Broadcast(MainWeapon);
	return RetVal;
}

AEnemyWeapon* AEnemyCharacter::DropSecondWeapon(bool DestroyWeapon)
{
	if (SecondWeapon == nullptr) return nullptr;
	
	if (DestroyWeapon)
	{		
		SecondWeapon->Destroy();
		return nullptr;
	}
	else 
	{
		AEnemyWeapon* RetVal = nullptr;
		
		if (IsValid(SecondWeapon))
		{
			SecondWeapon->DetachFromActor(FDetachmentTransformRules::KeepWorldTransform);
			SecondWeapon->SetOwner(nullptr);
			SecondWeapon->SetInstigator(nullptr);
			SecondWeapon->OnDrop();
			
			RetVal = SecondWeapon;
		}
		
		SecondWeapon = nullptr;
		
		return RetVal;
	}
}

AEnemyWeapon* AEnemyCharacter::InitializeDefaultMainWeapon()
{
	auto Res = SetMainWeapon(nullptr,DefaultMainWeaponClass);
	return Res;
}

AEnemyWeapon* AEnemyCharacter::InitializeDefaultSecondWeapon()
{
	auto Res = SetSecondWeapon(nullptr, DefaultSecondWeaponClass);
	return Res;
}

void AEnemyCharacter::SwitchWeapon()
{
	auto WeaponM = DropMainWeapon(false);
	auto WeaponS = DropSecondWeapon(false);
	SetMainWeapon(WeaponS);
	SetSecondWeapon(WeaponM);
}

void AEnemyCharacter::OnHealthChange(UActorStatus* Status)
{
}

bool AEnemyCharacter::GetMuzzleLocation(FVector& MuzzleLocation)
{
	if (IsValid(MainWeapon))
	{
		MuzzleLocation = MainWeapon->GetMuzzleLocation();
		return true;
	}
	
	return false;

}

void AEnemyCharacter::OnCharacterMovementUpdatedFunction(float DeltaSeconds, FVector OldLocation, FVector OldVelocity)
{
	//if (Aiming)	AimToPoint(AimPoint);
	//if (Tracking) TrackPoint(TrackingPoint);
}

