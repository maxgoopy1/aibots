// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"

#include "Components/BoxComponent.h"
#include "GameFramework/Actor.h"
#include "PatrolArea.generated.h"

USTRUCT()
struct AIBOTS_API FPatrolZone
{
	GENERATED_BODY()

	//Single patrol box
	UPROPERTY(Instanced)
	UBoxComponent* PatrolBox;

	//Weight/priority of patrol box
	UPROPERTY()
	float Weight = 1;

	//Zones Key
	UPROPERTY()
	float Key = 1;
};


UCLASS()
class AIBOTS_API APatrolArea : public AActor
{
	GENERATED_BODY()

	//Array of patrol zones
	UPROPERTY()
	TArray<FPatrolZone> PatrolZones;
	
public:	
	// Sets default values for this actor's properties 
	APatrolArea();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	
public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	//Return default/first patrol area
	UBoxComponent* GetPatrolArea()	const;

	//Return a random point in array of PatrolZones
	UFUNCTION(BlueprintPure,Category="Patrolling")
	FVector GetRandomPointInPatrolArea() const;

	//Update Key and Weight of PatrolZones
	UFUNCTION()
	void UpdatePatrolZones( int StartIndex = 0);

	//Add PatrolZones from other PatrolArea
	UFUNCTION()
	void AddPatrolArea(APatrolArea* Other);

	//Add all PatrolArea from Array to first item in Array
	UFUNCTION(BlueprintCallable)
	static APatrolArea* CombinePatrolAreas(TArray<APatrolArea*> PatrolAreas);
};
