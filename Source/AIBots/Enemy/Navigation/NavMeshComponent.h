// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"

#include "Chaos/AABBTree.h"
#include "Components/BoxComponent.h"
#include "NavMesh/NavMeshBoundsVolume.h"

#include "NavMeshComponent.generated.h"

/**
 * 
 */
UCLASS(Blueprintable)
class AIBOTS_API UNavMeshComponent : public UBoxComponent
{
	GENERATED_BODY()

	


public:
	UNavMeshComponent();

	UFUNCTION(BlueprintCallable)
	void UpdateNavMesh();

	virtual void BeginPlay() override;
	UPROPERTY(EditAnywhere,BlueprintReadWrite)
	ANavMeshBoundsVolume* NavMesh = nullptr;
	
};
