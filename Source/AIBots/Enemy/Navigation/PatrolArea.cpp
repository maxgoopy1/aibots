// Fill out your copyright notice in the Description page of Project Settings.


#include "PatrolArea.h"

#include "MovieSceneSequenceID.h"
#include "Kismet/KismetMathLibrary.h"

// Sets default values
APatrolArea::APatrolArea()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = false;
	//Create Box for default patrol zone
	UBoxComponent* DefaultPatrolBox = CreateDefaultSubobject<UBoxComponent>(TEXT("PatrolArea"));
	SetRootComponent(DefaultPatrolBox);

	//Init Default Patrol zone and add to Array
	FPatrolZone DefaultPatrolZone{DefaultPatrolBox};
	PatrolZones.Add(DefaultPatrolZone);
	
}

// Called when the game starts or when spawned
void APatrolArea::BeginPlay()
{
	Super::BeginPlay();

	//Update PatrolZones values
	UpdatePatrolZones();
}

// Called every frame
void APatrolArea::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

UBoxComponent* APatrolArea::GetPatrolArea() const
{
	if (PatrolZones.Num()!=0)
	{
		UBoxComponent* BoxComp = PatrolZones[0].PatrolBox;
		return BoxComp;
	}
	return nullptr;
}

FVector APatrolArea::GetRandomPointInPatrolArea() const
{
	float Random = UKismetMathLibrary::RandomFloat();
	Random *= PatrolZones[PatrolZones.Num()-1].Key;

	const FPatrolZone* TargetZone = PatrolZones.FindByPredicate([Random](FPatrolZone Zone)
	{
		return Random < Zone.Key;
	});
	
	if (TargetZone && IsValid(TargetZone->PatrolBox))
	{
		UBoxComponent* BoxComp = TargetZone->PatrolBox;
	
		if (IsValid(BoxComp))
		{
			FVector BoxExtent = BoxComp->Bounds.BoxExtent;
			FVector BoxOrigin = BoxComp->Bounds.Origin;

			return UKismetMathLibrary::RandomPointInBoundingBox(BoxOrigin,
        BoxExtent);
		}
	}
	return GetActorLocation();
}

void APatrolArea::UpdatePatrolZones(int StartIndex)
{
	if (PatrolZones.Num() == 0 || PatrolZones.Num() <= StartIndex || StartIndex < 0) return;
	
	FVector BoxExtent;
	auto ZonePrevIter = PatrolZones.CreateIterator();
	ZonePrevIter+=StartIndex;
	auto ZoneIter = ZonePrevIter;

	if (StartIndex == 0)
	{
		//Update First item
		BoxExtent = ZoneIter->PatrolBox->Bounds.BoxExtent;
		ZoneIter->Weight = BoxExtent.X * BoxExtent.Y * BoxExtent.Z;
		ZoneIter->Key = ZoneIter->Weight;
		++ZoneIter;
	}
	else
	{
		--ZonePrevIter;
	}

	//Update next items
	for (; ZoneIter; ++ZoneIter, ++ZonePrevIter)
	{
		BoxExtent = ZoneIter->PatrolBox->Bounds.BoxExtent;
		ZoneIter->Weight = BoxExtent.X * BoxExtent.Y * BoxExtent.Z;
		ZoneIter->Key = ZonePrevIter->Key + ZoneIter->Weight;
	}
}

void APatrolArea::AddPatrolArea(APatrolArea* Other)
{
	if (Other == this ) return;
	
	//Reattach PatrolBox from Other
	for(auto PatrolZone : Other->PatrolZones)
	{
		PatrolZone.PatrolBox->AttachToComponent(RootComponent, FAttachmentTransformRules::KeepWorldTransform);
		
	}

	//Append Other PatrolZones to this
	int LastZoneIndex = PatrolZones.Num()-1;
	PatrolZones.Append(Other->PatrolZones);

	//Update new range of PatrolZones
	UpdatePatrolZones(LastZoneIndex);

	//Destroy Other
	//Other->Destroy();
}

APatrolArea* APatrolArea::CombinePatrolAreas(TArray<APatrolArea*> PatrolAreas)
{
	if (PatrolAreas.Num() == 0)
	{
		UE_LOG(LogTemp,Warning,TEXT("APatrolArea::CombinePatrolAreas called with empty Array"));
		return nullptr;
	}

	//Get first PatrolArea as Main
	APatrolArea* MainPA = PatrolAreas[0];

	if(!IsValid(MainPA)) 
	{
		UE_LOG(LogTemp,Error,TEXT("APatrolArea::CombinePatrolAreas called with Array which first element InValid"));
		return nullptr;
	}

	//Add all other PatrolAreas to MainPA
	auto PatrolAreaIter = PatrolAreas.CreateIterator();
	++PatrolAreaIter;
	for (;PatrolAreaIter; ++PatrolAreaIter)
	{
		if (IsValid(*PatrolAreaIter))
		MainPA->AddPatrolArea(*PatrolAreaIter);
	}

	return MainPA;
}

