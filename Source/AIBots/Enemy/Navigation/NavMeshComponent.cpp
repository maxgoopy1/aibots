// Fill out your copyright notice in the Description page of Project Settings.


#include "NavMeshComponent.h"


#include "NavigationSystem.h"
#include "Kismet/GameplayStatics.h"


UNavMeshComponent::UNavMeshComponent()
{
	// if (GetOwner())
	// {
	// 	NavMesh = CreateDefaultSubobject<ANavMeshBoundsVolume>(TEXT("NavMesh"));
	// }
}

void UNavMeshComponent::UpdateNavMesh()
{
	if (GetOwner() && GetWorld() && NavMesh)
	{
		FVector NewOrigin;
		FVector NewBoxExtent;
		GetOwner()->GetActorBounds(false, NewOrigin,NewBoxExtent, true);
		//Transform BoxComponent
		SetBoundsScale(1);
		SetBoxExtent(NewBoxExtent);
		SetWorldLocation(NewOrigin);
		//Transform Navmesh
		FTransform Transform = GetComponentTransform();
		FVector NavMeshBoxExtent;
		FVector NavMeshOrigin;
		NavMesh->SetActorScale3D(FVector::OneVector);
		NavMesh->GetActorBounds(false, NavMeshOrigin, NavMeshBoxExtent, true);
		FVector Scale;
		Scale = NewBoxExtent/NavMeshBoxExtent;
		Transform.SetScale3D(Scale);
		NavMesh->GetRootComponent()->SetMobility(EComponentMobility::Movable);
		NavMesh->SetActorTransform(Transform);
		NavMesh->GetRootComponent()->SetMobility(EComponentMobility::Static);
		UNavigationSystemV1::GetCurrent(GetWorld())->OnNavigationBoundsUpdated(NavMesh);
	}
}

void UNavMeshComponent::BeginPlay()
{
	Super::BeginPlay();
	if (!NavMesh)
	{
		NavMesh = Cast<ANavMeshBoundsVolume>(UGameplayStatics::GetActorOfClass(GetWorld(), ANavMeshBoundsVolume::StaticClass()));
		UpdateNavMesh();
	}
}

