// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "AIController.h"
#include "EnemyBehavior.h"
#include "Perception/AIPerceptionStimuliSourceComponent.h"
#include "Perception/AISenseConfig_Damage.h"
#include "Perception/AISenseConfig_Hearing.h"
#include "Perception/AISenseConfig_Sight.h"
#include "EnemyController.generated.h"


DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnBehaviorChangedEvent, EEnemyBehavior, Behavior);
/**
 * 
 */
UCLASS(BlueprintType, Blueprintable)
class AIBOTS_API AEnemyController : public AAIController
{
	GENERATED_BODY()


private:

	UPROPERTY(EditDefaultsOnly, Instanced)
	UAISenseConfig_Sight* SenseConfig_Sight;

	UPROPERTY(EditDefaultsOnly, Instanced)
	UAISenseConfig_Damage* SenseConfig_Damage;
	
	UPROPERTY(EditDefaultsOnly, Instanced)
	UAISenseConfig_Hearing* SenseConfig_Hearing;
	
	UPROPERTY(EditAnywhere)
	class UBehaviorTree* AIBehavior;
	
public:
	
	AEnemyController(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get());

	virtual void BeginDestroy() override;

	virtual void OnConstruction(const FTransform& Transform) override;
	
	//Set pawn MovementComponent->bOrientRotationToMovement
	UFUNCTION(BlueprintCallable)
    void SetOrientRotationToMovement(bool orientToMovement);

	//If Actor can be a target
	UFUNCTION(BlueprintCallable, BlueprintPure)
	bool IsValidTarget(AActor* Actor);

	UPROPERTY(BlueprintAssignable, BlueprintCallable)
	FOnBehaviorChangedEvent OnBehaviorChanged;


protected:
	virtual void BeginPlay() override;

	virtual void OnPossess(APawn* InPawn) override;

	//Catch event Perception->OnTargetPerceptionUpdated 
	UFUNCTION(BlueprintNativeEvent)
	void OnTargetPerceptionUpdated( AActor* Actor, FAIStimulus Stimulus);

	//On perception system send Sight Stimulus
	UFUNCTION(BlueprintNativeEvent)
	void OnStimulusSight( AActor* Actor, FAIStimulus Stimulus);

	//On perception system send Damage Stimulus
	UFUNCTION(BlueprintNativeEvent)
    void OnStimulusDamage( AActor* Actor, FAIStimulus Stimulus);

	//On perception system send Hearing Stimulus
	UFUNCTION(BlueprintNativeEvent)
    void OnStimulusHearing( AActor* Actor, FAIStimulus Stimulus);
	
	
};
