// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"

#include "AISenseSystem.h"
#include "Perception/AIPerceptionComponent.h"

#include "AISenseSystemPerception.generated.h"


UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class AIBOTS_API UAISenseSystemPerception : public UAISenseSystem
{
	GENERATED_BODY()
private:
	UPROPERTY()
	UAIPerceptionComponent* PerceptionComponent;

public:
	// Sets default values for this component's properties
	UAISenseSystemPerception(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get());

	UAIPerceptionComponent* GetPerceptionComponent() const { return PerceptionComponent; };
protected:
	// Called when the game starts
	virtual void BeginPlay() override;

public:
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType,
	                           FActorComponentTickFunction* ThisTickFunction) override;

	virtual void OnRegister() override;
	virtual void PostInitProperties() override;
	virtual void AddSense(UAISenseBase* Sense, int Priority) override;
};
