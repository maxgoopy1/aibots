// Fill out your copyright notice in the Description page of Project Settings.


#include "AISenseSystemBot.h"

#include "../Senses/AISensePerceptionSight.h"
#include "Perception/AISenseConfig_Sight.h"


UAISenseSystemBot::UAISenseSystemBot(const FObjectInitializer& ObjectInitializer) : Super(ObjectInitializer)
{	
	SenseSight = CreateDefaultSubobject<UAISensePerceptionSight>(TEXT("SenseSight"));
	SenseHearing = CreateDefaultSubobject<UAISensePerceptionHearing>(TEXT("SenseHearing"));
	SenseDamage = CreateDefaultSubobject<UAISensePerceptionDamage>(TEXT("SenseDamage"));
}

void UAISenseSystemBot::BeginPlay()
{
	Super::BeginPlay();
}

void UAISenseSystemBot::OnRegister()
{
	Super::OnRegister();

	AddSense(SenseSight,	10);
	AddSense(SenseHearing,	1);
	AddSense(SenseDamage,	5);
	
	if(GetPerceptionComponent() != nullptr)
	{
		auto DominantConfig = SenseSight->GetSenseConfig();
		if(DominantConfig)
		{
			GetPerceptionComponent()->SetDominantSense(DominantConfig->GetSenseImplementation());
			GetPerceptionComponent()->RequestStimuliListenerUpdate();
		}
	}
}

void UAISenseSystemBot::TickComponent(float DeltaTime, ELevelTick TickType,
	FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);
}

