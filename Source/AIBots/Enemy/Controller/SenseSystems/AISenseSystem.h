// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"

#include "AIController.h"
#include "../Senses/AISenseBase.h"
#include "Chaos/AABBTree.h"
#include "UObject/NoExportTypes.h"
#include "AISenseSystem.generated.h"


USTRUCT(BlueprintType)
struct FEnemyInfo
{
	GENERATED_BODY()

	//Pointer to enemy Actor
	UPROPERTY(BlueprintReadOnly)
	AActor* Enemy;

	//Last registered by stronger Sense enemy world Transform
	UPROPERTY(BlueprintReadOnly)
	FTransform LastKnownEnemyTransform;

	//Sense Flags which sensing Enemy
	UPROPERTY(BlueprintReadOnly)
	ESense SenseFlag;

	//Amount of Aggression caused from Enemy
	UPROPERTY(BlueprintReadOnly)
	float Aggression;
};

//Pair of ESense and int for determinate Sense priority
USTRUCT()
struct FSensePriority
{
	GENERATED_BODY()
	ESense SenseFlag;
	int Priority;
};

/**
* Sense Facade for several AISense realisation
*/
UCLASS()
class AIBOTS_API UAISenseSystem : public UActorComponent
{
	GENERATED_BODY()

protected:
	//Map of sensing Enemy and there FEnemyInfo
	UPROPERTY()
	TMap<const AActor*,FEnemyInfo> SensingEnemyMap;

	//Senses working by events (Hearing, Damage) 
	UPROPERTY()
	TArray<UAISenseBase*> EventSenses;

	//Senses required updating (Sight)
	UPROPERTY()
	TArray<UAISenseBase*> UpdatableSenses;

	//Array of Sense priority
	UPROPERTY()
	TArray<FSensePriority> SensesPriority;

	//AIController to which we attach
	UPROPERTY()
	AAIController* AIController;

private:
	/**
	 * Compared Mask and Value and return if the current Value  has highest priority in the Mask
	 */
	bool IsPriorityForMask(ESense Mask, ESense Value);


	//Update Senses which required updating
	void UpdateSenses(float DeltaTime);

public:
	UAISenseSystem(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get());
	virtual void BeginPlay() override;
	
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

	virtual void OnRegister() override;
	
	virtual void InitAIController(AAIController* Controller = nullptr);
	/**
	 *Detected Enemy by some Sense
	 *@param Enemy - detected enemy
	 *@param EnemyInfo - sense enemy info
	 */
	UFUNCTION()
	void SensedEnemy(AActor* Enemy, FEnemyInfo& EnemyInfo);

	/**
     *Lost Enemy by some Sense
     *@param Enemy - lost enemy
	 *@param SenseFlag - sense flag
     */
	UFUNCTION()
	void UnSensedEnemy( AActor* Enemy, ESense SenseFlag);

	//Delete enemy from Senses
	UFUNCTION()
	void OnEnemyDestroyed( AActor* Enemy);

	UFUNCTION()
	virtual void AddSense(UAISenseBase* Sense, int Priority);

	AAIController* GetAIController(){return AIController;}

	virtual bool IsEnemy(AActor* Actor);

	/**
	 * Get FEnemyIfo about Enemy, if that Sensed
	 * @param Enemy - Actor which Info we find
	 * @param OutInfo - Output pointer on FEnemyInfo
	 * @return True if that Actor sensed, else false 
	 */
	bool GetEnemyInfo(AActor* Enemy, const FEnemyInfo*& OutInfo ) const;

	
};

