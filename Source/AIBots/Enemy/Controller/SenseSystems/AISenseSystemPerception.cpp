// Fill out your copyright notice in the Description page of Project Settings.


#include "AISenseSystemPerception.h"

#include "../Senses/AISensePerception.h"


UAISenseSystemPerception::UAISenseSystemPerception(const FObjectInitializer& ObjectInitializer) : Super(ObjectInitializer)
{
	PrimaryComponentTick.bCanEverTick = true;
}


void UAISenseSystemPerception::BeginPlay()
{
	Super::BeginPlay();

	/*if (GetPerceptionComponent() == nullptr)
		return;
	for (auto Sense : EventSenses)
	{
		auto SensePerception = Cast<UAISensePerception>(Sense);
		if(SensePerception)
			SensePerception->BindPerceptionComponent(GetPerceptionComponent());
	}
	for (auto Sense : UpdatableSenses)
	{
		auto SensePerception = Cast<UAISensePerception>(Sense);
		if(SensePerception)
			SensePerception->BindPerceptionComponent(GetPerceptionComponent());
	}*/
}


void UAISenseSystemPerception::TickComponent(float DeltaTime, ELevelTick TickType,
                                             FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);
}

void UAISenseSystemPerception::OnRegister()
{
	Super::OnRegister();

	if(GetAIController() == nullptr)
		return;
	
	PerceptionComponent = GetAIController()->GetPerceptionComponent();
	if (!PerceptionComponent)
	{
		GetAIController()->AddComponentByClass(UAIPerceptionComponent::StaticClass(),false,FTransform::Identity,false);
		PerceptionComponent = GetAIController()->GetPerceptionComponent();
	}
}

void UAISenseSystemPerception::PostInitProperties()
{
	Super::PostInitProperties();

	
}

void UAISenseSystemPerception::AddSense(UAISenseBase* Sense, int Priority)
{
	Super::AddSense(Sense, Priority);
	
	if (GetPerceptionComponent() == nullptr)
		return;

	if(GetPerceptionComponent()!= nullptr)
	{
		auto SensePerception = Cast<UAISensePerception>(Sense);
		if (SensePerception)
		{
			GetPerceptionComponent()->ConfigureSense(*SensePerception->GetSenseConfig());
			SensePerception->BindPerceptionComponent(GetPerceptionComponent());
		}
	}
}

