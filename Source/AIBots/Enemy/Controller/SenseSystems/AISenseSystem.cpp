// Fill out your copyright notice in the Description page of Project Settings.


#include "AISenseSystem.h"

#include "GameFramework/Character.h"

bool UAISenseSystem::IsPriorityForMask(ESense Mask, ESense Value)
{
	//Find ValuePair in Array
	auto ValuePair = SensesPriority.FindByPredicate([Value](FSensePriority SensePriority)
	{
		return Value == SensePriority.SenseFlag;
	});
	
	//Value not contain in Array
	if (!ValuePair)
		return false;

	//Find highest priority pair which matching Mask
	auto HighestPriorityPair = SensesPriority.FindByPredicate([Mask](FSensePriority SensePriority)
	{
		return ( Mask & SensePriority.SenseFlag) == SensePriority.SenseFlag;
	});

	//No senses matching Mask, that mean any Value has highest priority for Mask
	if (!HighestPriorityPair)
		return true;

	//Compare ValuePair and HighestPriorityPair
	return ValuePair->SenseFlag >= HighestPriorityPair->SenseFlag;
}

void UAISenseSystem::UpdateSenses(float DeltaTime)
{
	for (auto Sense : UpdatableSenses)
	{
		Sense->UpdateExternal(DeltaTime);
	}
}

UAISenseSystem::UAISenseSystem(const FObjectInitializer& ObjectInitializer) : Super(ObjectInitializer)
{
	PrimaryComponentTick.bCanEverTick = true;
}

void UAISenseSystem::BeginPlay()
{
	Super::BeginPlay();

}

void UAISenseSystem::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	UpdateSenses(DeltaTime);
}

void UAISenseSystem::OnRegister()
{
	Super::OnRegister();

	
	if (GetOuter())
	{
		auto Controller = Cast<AAIController>(GetOuter());
		if (Controller)
			AIController = Controller;
		else
		{
			auto Character = Cast<ACharacter>(GetOuter());
			if (IsValid(Character))
				AIController = Cast<AAIController>(Character->Controller);
		
		}
	}
}

void UAISenseSystem::InitAIController(AAIController* NewController)
{
	if (IsValid(NewController))
	{
		AIController = NewController;
		return;
	}
	
	if (IsValid(GetOwner()))
	{
		auto Controller = Cast<AAIController>(GetOwner());
		if (Controller)
			AIController = Controller;
		else
		{
			auto Character = Cast<ACharacter>(GetOwner());
			if (IsValid(Character))
				AIController = Cast<AAIController>(Character->Controller);
		
		}
	}
	//check(IsValid(AIController));
}

void UAISenseSystem::SensedEnemy(AActor* Enemy,  FEnemyInfo& EnemyInfo)
{
	if (!IsValid(Enemy))
		return;
	
	if (!SensingEnemyMap.Contains(Enemy))
	{
		//Add new Enemy
		SensingEnemyMap.Add(Enemy,EnemyInfo);
		Enemy->OnDestroyed.AddDynamic(this, &UAISenseSystem::OnEnemyDestroyed);
	}
	else
	{
		//Update EnemyInfo
		auto OldInfo = &SensingEnemyMap[Enemy];
		OldInfo->SenseFlag |= EnemyInfo.SenseFlag;
		OldInfo->Aggression+=EnemyInfo.Aggression;
		if (IsPriorityForMask(OldInfo->SenseFlag, EnemyInfo.SenseFlag))
		{
			OldInfo->LastKnownEnemyTransform = EnemyInfo.LastKnownEnemyTransform;
		}
	}
}

void UAISenseSystem::UnSensedEnemy( AActor* Enemy, ESense SenseFlag)
{
	if (SensingEnemyMap.Contains(Enemy))
	{
		auto EnemyInfo = &SensingEnemyMap[Enemy];
		EnemyInfo->SenseFlag &= ~SenseFlag;
		if (EnemyInfo->SenseFlag == ESense::NOSENSE)
			SensingEnemyMap.Remove(Enemy);
		if(IsValid(Enemy))
			Enemy->OnDestroyed.RemoveDynamic(this, &UAISenseSystem::OnEnemyDestroyed);
	}
}

void UAISenseSystem::OnEnemyDestroyed(AActor* Enemy)
{
	UnSensedEnemy(Enemy, ESense::ALL);
}

void UAISenseSystem::AddSense(UAISenseBase* Sense, int Priority)
{
	if (!IsValid(Sense))
		return;
	
	if(Sense->GetUpdatable())
	{
		UpdatableSenses.Add(Sense);
	}
	else
	{
		EventSenses.Add(Sense);
	}
	
	SensesPriority.Add({Sense->GetSenseFlag(), Priority});
	Sense->SetSenseSystem(this);
}

bool UAISenseSystem::IsEnemy(AActor* Actor)
{
	return IsValid(Actor) && Actor->ActorHasTag(FName(TEXT("Player")));
}

bool UAISenseSystem::GetEnemyInfo(AActor* Enemy, const FEnemyInfo*& OutInfo) const
{
	if (SensingEnemyMap.Contains(Enemy))
	{
		OutInfo = SensingEnemyMap.Find(Enemy);
		return true;
	}
	
	return false;
}


