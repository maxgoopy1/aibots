// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"

#include "../Senses/AISensePerceptionSight.h"
#include "AISenseSystemPerception.h"
#include "AIBots/Enemy/Controller/Senses/AISensePerceptionDamage.h"
#include "AIBots/Enemy/Controller/Senses/AISensePerceptionHearing.h"


#include "AISenseSystemBot.generated.h"

/**
 * Sense System for Bot
 */
UCLASS(Blueprintable, ClassGroup=(Custom), meta=(BlueprintSpawnableComponent))
class AIBOTS_API UAISenseSystemBot : public UAISenseSystemPerception
{
	GENERATED_BODY()

	UPROPERTY(EditDefaultsOnly, Instanced, Category=AI)
	UAISensePerceptionSight* SenseSight;

	UPROPERTY(EditDefaultsOnly, Instanced, Category=AI)
	UAISensePerceptionHearing* SenseHearing;
	
	UPROPERTY(EditDefaultsOnly, Instanced, Category=AI)
	UAISensePerceptionDamage* SenseDamage;

	
public:
	UAISenseSystemBot(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get());

	virtual void BeginPlay() override;
	virtual void OnRegister() override;
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;
};
