// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"

#include "AISensePerception.h"
#include "Perception/AISenseConfig_Damage.h"

#include "AISensePerceptionDamage.generated.h"

/**
 * 
 */
UCLASS()
class AIBOTS_API UAISensePerceptionDamage : public UAISensePerception
{
	GENERATED_BODY()

	UPROPERTY(Instanced)
	UAISenseConfig_Damage* SenseConfig_Damage;

	public:
	UAISensePerceptionDamage(const FObjectInitializer& ObjectInitializer);

	virtual void OnTargetPerceptionUpdated(AActor* Actor, FAIStimulus Stimulus) override;
};

