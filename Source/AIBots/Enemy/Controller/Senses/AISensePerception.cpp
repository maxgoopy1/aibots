// Fill out your copyright notice in the Description page of Project Settings.


#include "AISensePerception.h"


#include "../SenseSystems/AISenseSystem.h"
#include "Perception/AIPerceptionComponent.h"
#include "Perception/AISenseConfig_Sight.h"

UAISensePerception::UAISensePerception(const FObjectInitializer& ObjectInitializer) : Super(ObjectInitializer)
{
	
}

void UAISensePerception::BindPerceptionComponent(UAIPerceptionComponent* PerceptionComponent)
{
	
	if (PerceptionComponent && !PerceptionComponent->IsPendingKill())
	{
		//Bind Events
		PerceptionComponent->OnTargetPerceptionUpdated.AddDynamic(this, &UAISensePerception::OnTargetPerceptionUpdated);

		//Add to Array
		if (!PerceptionComponents.Contains(PerceptionComponent))
			PerceptionComponents.Add(PerceptionComponent);
	}
}

void UAISensePerception::UnBindPerceptionComponent(UAIPerceptionComponent* PerceptionComponent)
{
	if (PerceptionComponent && !PerceptionComponent->IsPendingKill())
	{
		//Unbind Events
		PerceptionComponent->OnTargetPerceptionUpdated.RemoveDynamic(this, &UAISensePerception::OnTargetPerceptionUpdated);

		//Remove from Array
		PerceptionComponents.Remove(PerceptionComponent);
	}
}

void UAISensePerception::OnTargetPerceptionUpdated(AActor* Actor, FAIStimulus Stimulus)
{
	if (!SenseConfig)
		return;
	
	if (Stimulus.Type == SenseConfig->GetSenseID())
	{
		
		if (Stimulus.WasSuccessfullySensed())
		{
			FEnemyInfo Info{Actor, Actor->GetTransform(), SenseFlag, 0};
			SenseSystem->SensedEnemy(Actor,Info);
		}
		else
		{
			SenseSystem->UnSensedEnemy(Actor,SenseFlag);
		}
	}
}
