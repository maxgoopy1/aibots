// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"

#include "AISenseBase.h"
#include "Perception/AISenseConfig.h"

#include "AISensePerception.generated.h"

/**
 * Base Class for Senses working with PerceptionComponent
 */
UCLASS(Abstract)
class AIBOTS_API UAISensePerception : public UAISenseBase
{
	GENERATED_BODY()

protected:
	//Sense Config for current Sense
	UPROPERTY(EditDefaultsOnly, Instanced)
	UAISenseConfig* SenseConfig;

	//Array of listening perception component 
	UPROPERTY()
	TArray<UAIPerceptionComponent*> PerceptionComponents;
	
public:	
	UAISensePerception(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get());

	UAISenseConfig* GetSenseConfig(){return SenseConfig;}

	/**
	 *Bind sense to given PerceptionComponent
	 *Potentially Sense can be bind on several PerceptionalComponent
	*/
	void BindPerceptionComponent(UAIPerceptionComponent* PerceptionComponent);

	/**
	 *Unbind Sense from given PerceptionalComponent
	 */
	void UnBindPerceptionComponent(UAIPerceptionComponent* PerceptionComponent);

	UFUNCTION()
	virtual void OnTargetPerceptionUpdated(AActor* Actor, FAIStimulus Stimulus);
};
