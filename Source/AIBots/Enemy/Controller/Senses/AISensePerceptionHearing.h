// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"

#include "AISensePerception.h"
#include "Perception/AISenseConfig_Hearing.h"

#include "AISensePerceptionHearing.generated.h"

/**
 * Sense for Perception Hear
 */
UCLASS()
class AIBOTS_API UAISensePerceptionHearing : public UAISensePerception
{
	GENERATED_BODY()

	UPROPERTY(Instanced)
	UAISenseConfig_Hearing* SenseConfig_Hearing;

	public:
	UAISensePerceptionHearing(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get());
	virtual void OnTargetPerceptionUpdated(AActor* Actor, FAIStimulus Stimulus) override;
};
