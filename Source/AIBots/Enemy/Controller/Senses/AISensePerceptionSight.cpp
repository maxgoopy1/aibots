// Fill out your copyright notice in the Description page of Project Settings.


#include "AISensePerceptionSight.h"



#include "../SenseSystems/AISenseSystem.h"
#include "Perception/AIPerceptionComponent.h"
#include "Perception/AISenseConfig_Sight.h"

float UAISensePerceptionSight::CalculateAggression(AActor* Actor)
{
	const FEnemyInfo* Info;
	float Aggression =0;
	
	//If we previously sensed this Actor, then dont change Aggression (It mast be already set)
	bool bAlreadySensed = SenseSystem->GetEnemyInfo(Actor, Info) && ((Info->SenseFlag & SenseFlag) == SenseFlag);
	if (!bAlreadySensed)
	{
		Aggression = SenseSystem->IsEnemy(Actor)? 100 : 0;
	}
	
	return Aggression;
}

UAISensePerceptionSight::UAISensePerceptionSight(const FObjectInitializer& ObjectInitializer) : Super(ObjectInitializer)
{
	SenseSightConfig = CreateDefaultSubobject<UAISenseConfig_Sight>(TEXT("SightConfig"));
	SenseSightConfig->SightRadius = 10000;
	SenseSightConfig->LoseSightRadius = 15000;
	SenseSightConfig->PeripheralVisionAngleDegrees = 80;
	SenseSightConfig->SetMaxAge(10);
	SenseSightConfig->DetectionByAffiliation.bDetectEnemies = true;
	SenseSightConfig->DetectionByAffiliation.bDetectFriendlies = true;
	SenseSightConfig->DetectionByAffiliation.bDetectNeutrals = true;

	SenseConfig = SenseSightConfig;
	SenseFlag =  ESense::SIGHT;

	Updatable = true;
}

void UAISensePerceptionSight::OnTargetPerceptionUpdated(AActor* Actor, FAIStimulus Stimulus)
{
	if (!SenseConfig)
		return;
	
	if (Stimulus.Type == SenseConfig->GetSenseID())
	{
		
		if (Stimulus.WasSuccessfullySensed())
		{
			FEnemyInfo Info{Actor, Actor->GetTransform(), SenseFlag, CalculateAggression(Actor)};
			SenseSystem->SensedEnemy(Actor,Info);
		}
		else
		{
			SenseSystem->UnSensedEnemy(Actor,SenseFlag);
		}
	}
}

void UAISensePerceptionSight::UpdateExternal(float DeltaTime)
{
	if (DeltaTime <=  0)
		return;

	TArray<AActor*> Actors;
	for (auto PComp : PerceptionComponents)
	{
		PComp->GetCurrentlyPerceivedActors(SenseConfig->GetSenseImplementation(),Actors);
	}

	for(auto Actor : Actors)
	{
		//Aggression always 0 (It mast be set by Event  OnTargetPerceptionUpdated)
		FEnemyInfo Info{Actor, Actor->GetTransform(), SenseFlag, 0};
		SenseSystem->SensedEnemy(Actor,Info);
	}
}


