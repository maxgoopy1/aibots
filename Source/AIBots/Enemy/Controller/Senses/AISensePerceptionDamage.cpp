// Fill out your copyright notice in the Description page of Project Settings.


#include "AISensePerceptionDamage.h"

#include "AIBots/Enemy/Controller/SenseSystems/AISenseSystem.h"

UAISensePerceptionDamage::UAISensePerceptionDamage(const FObjectInitializer& ObjectInitializer) : Super(ObjectInitializer)
{
	SenseConfig_Damage = CreateDefaultSubobject<UAISenseConfig_Damage>(TEXT("SenseConfigDamage"));
	if(SenseConfig_Damage)
	{
		SenseConfig_Damage->SetMaxAge(10);
	}

	SenseConfig = SenseConfig_Damage;
	
	SenseFlag = ESense::DAMAGE;
}

void UAISensePerceptionDamage::OnTargetPerceptionUpdated(AActor* Actor, FAIStimulus Stimulus)
{
	if (!SenseConfig)
		return;
	
	if (Stimulus.Type == SenseConfig->GetSenseID())
	{
		
		if (Stimulus.WasSuccessfullySensed())
		{
			FEnemyInfo Info{Actor, Actor->GetTransform(), SenseFlag, Stimulus.Strength};
			SenseSystem->SensedEnemy(Actor,Info);
		}
		else
		{
			SenseSystem->UnSensedEnemy(Actor,SenseFlag);
		}
	}
}
