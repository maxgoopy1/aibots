// Fill out your copyright notice in the Description page of Project Settings.


#include "AISensePerceptionHearing.h"


#include "AIBots/Enemy/Controller/SenseSystems/AISenseSystem.h"
#include "Perception/AISenseConfig_Sight.h"

UAISensePerceptionHearing::UAISensePerceptionHearing(const FObjectInitializer& ObjectInitializer) : Super(ObjectInitializer)
{
	SenseConfig_Hearing = CreateDefaultSubobject<UAISenseConfig_Hearing>(TEXT("SenseConfigHearing"));
	if (SenseConfig_Hearing)
	{
		SenseConfig_Hearing->HearingRange = 500;
		SenseConfig_Hearing->DetectionByAffiliation.bDetectEnemies = true;
		SenseConfig_Hearing->DetectionByAffiliation.bDetectFriendlies = true;
		SenseConfig_Hearing->DetectionByAffiliation.bDetectNeutrals = true;
		SenseConfig_Hearing->SetMaxAge(10);
	}
	
	SenseConfig = SenseConfig_Hearing;

	SenseFlag = ESense::HEAR;
}

void UAISensePerceptionHearing::OnTargetPerceptionUpdated(AActor* Actor, FAIStimulus Stimulus)
{
	if (!SenseConfig)
		return;
	
	if (Stimulus.Type == SenseConfig->GetSenseID())
	{
		
		if (Stimulus.WasSuccessfullySensed())
		{
			FEnemyInfo Info{Actor, Actor->GetTransform(), SenseFlag, 0};
			SenseSystem->SensedEnemy(Actor,Info);
		}
		else
		{
			SenseSystem->UnSensedEnemy(Actor,SenseFlag);
		}
	}
}
