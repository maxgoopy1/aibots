// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "AISensePerception.h"
#include "Perception/AISenseConfig_Sight.h"

#include "AISensePerceptionSight.generated.h"

/**
 * Sense for Perception Sight
 */
UCLASS()
class AIBOTS_API UAISensePerceptionSight : public UAISensePerception
{
	GENERATED_BODY()

	UPROPERTY(Instanced)
	UAISenseConfig_Sight* SenseSightConfig;

private:
	float CalculateAggression(AActor* Actor);
public:
	UAISensePerceptionSight(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get());

	virtual void OnTargetPerceptionUpdated(AActor* Actor, FAIStimulus Stimulus) override;
	virtual void UpdateExternal(float DeltaTime) override;
};
