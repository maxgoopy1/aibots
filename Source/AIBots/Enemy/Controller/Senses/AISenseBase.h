// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "AISenseBase.generated.h"

class UAISenseSystem;

UENUM(meta = (BitFlags))
enum class ESense : uint8
{
	NOSENSE		= 0x00,
    SIGHT		= 0x01 << 0,
    HEAR		= 0x01 << 1,
    DAMAGE		= 0x01 << 2,
    ALL			= 0xFF
};
ENUM_CLASS_FLAGS(ESense);

/**
 * Base class for Senses
 */
UCLASS(Abstract)
class AIBOTS_API UAISenseBase : public UObject
{
	GENERATED_BODY()
protected:
	//Sense required Updating
	UPROPERTY()
	bool Updatable;

	//SenseSystem
	UPROPERTY()
	UAISenseSystem* SenseSystem;

	//Sense flag
	UPROPERTY()
	ESense SenseFlag;
public:
	bool GetUpdatable() const { return Updatable;}

	ESense GetSenseFlag() const {return SenseFlag;}

	//Function for external call Update
	virtual void UpdateExternal(float DeltaTime){}

	UFUNCTION()
	void SetSenseSystem(UAISenseSystem* NewSenseSystem){ SenseSystem = NewSenseSystem; }
	
};
