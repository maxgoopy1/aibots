// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "AIBots/Items/Weapon/EnemyWeapon.h"

#include "EnemyWeaponController.generated.h"


UCLASS(Blueprintable, ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class AIBOTS_API UEnemyWeaponController : public UActorComponent
{
	GENERATED_BODY()
public:
	UPROPERTY(BlueprintReadWrite)
	EWeaponType WeaponType = EWeaponType::FIREARMS;

	UPROPERTY(BlueprintReadWrite)
	EWeaponCategory WeaponCategory = EWeaponCategory::RIFLE;
	
public:	
	// Sets default values for this component's properties
	UEnemyWeaponController();

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

	UFUNCTION(BlueprintImplementableEvent, BlueprintCallable)
	void SetWeapon(AEnemyWeapon* NewWeapon);

	

};
