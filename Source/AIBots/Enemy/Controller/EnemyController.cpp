// Fill out your copyright notice in the Description page of Project Settings.


#include "EnemyController.h"


#include "../EnemyCharacter.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "Navigation/CrowdFollowingComponent.h"
#include "Perception/AIPerceptionComponent.h"

AEnemyController::AEnemyController(const FObjectInitializer& ObjectInitializer) : Super(ObjectInitializer.SetDefaultSubobjectClass<UCrowdFollowingComponent>(TEXT("PathFollowingComponent")))
{	
	//Create Perception component
	UAIPerceptionComponent* PercepComp = CreateDefaultSubobject<UAIPerceptionComponent>("PerceptionComponent");
	
	//Create Sight config
	SenseConfig_Sight = CreateDefaultSubobject<UAISenseConfig_Sight>(TEXT("ConfigSight"));
	
	//Create Damage Config
	SenseConfig_Damage = CreateDefaultSubobject<UAISenseConfig_Damage>(TEXT("ConfigDamage"));
	//Create Hearing Config
	SenseConfig_Hearing = CreateDefaultSubobject<UAISenseConfig_Hearing>(TEXT("ConfigHearing"));
	//Setting Sight Config
	SenseConfig_Sight->SightRadius = 10000;
	SenseConfig_Sight->LoseSightRadius = 15000;
	SenseConfig_Sight->PeripheralVisionAngleDegrees = 80;
	SenseConfig_Sight->DetectionByAffiliation.bDetectEnemies = true;
	SenseConfig_Sight->DetectionByAffiliation.bDetectFriendlies = true;
	SenseConfig_Sight->DetectionByAffiliation.bDetectNeutrals = true;
	

	//Setting Hearing Config
	SenseConfig_Hearing->HearingRange = 3000;
	SenseConfig_Hearing->DetectionByAffiliation.bDetectEnemies = true;
	SenseConfig_Hearing->DetectionByAffiliation.bDetectFriendlies = true;
	SenseConfig_Hearing->DetectionByAffiliation.bDetectNeutrals = true;

	if(PercepComp)
	{
		PercepComp->ConfigureSense(*SenseConfig_Sight);
		PercepComp->ConfigureSense(*SenseConfig_Damage);
		PercepComp->ConfigureSense(*SenseConfig_Hearing);
		PercepComp->SetDominantSense(SenseConfig_Sight->GetSenseImplementation());
		SetPerceptionComponent(*PercepComp);
	}

}

void AEnemyController::BeginDestroy()
{
	//Clear any active timer of actor
	auto World = GetWorld();
	if(World)
		World->GetTimerManager().ClearAllTimersForObject(this);

	Super::BeginDestroy();
}


void AEnemyController::OnConstruction(const FTransform& Transform)
{
	Super::OnConstruction(Transform);	
}

void AEnemyController::BeginPlay()
{
	Super::BeginPlay();

	if (!SenseConfig_Sight)
		UE_LOG(LogTemp,Warning,TEXT("Config_Sight was deleted in %s"), *GetName());
	if (!SenseConfig_Damage)
		UE_LOG(LogTemp,Warning,TEXT("Config_Damage was deleted in %s"), *GetName());
	if (!SenseConfig_Hearing)
		UE_LOG(LogTemp,Warning,TEXT("Config_Hearing was deleted in %s"), *GetName());

	//Setting Perception Component
	/*if(GetPerceptionComponent())
	{
		GetPerceptionComponent()->ConfigureSense(*SenseConfig_Sight);
		GetPerceptionComponent()->ConfigureSense(*SenseConfig_Damage);
		GetPerceptionComponent()->ConfigureSense(*SenseConfig_Hearing);
		GetPerceptionComponent()->SetDominantSense(SenseConfig_Sight->GetSenseImplementation());
	}*/
}

void AEnemyController::OnPossess(APawn* InPawn)
{
	Super::OnPossess(InPawn);

	//Run Behavior tree
	if(AIBehavior)
	{
		RunBehaviorTree(AIBehavior);
	}

	//Bind on event Perception->OnTargetPerceptionUpdated
	UAIPerceptionComponent* Perception = GetPerceptionComponent();
	if (Perception)
	{
		Perception->OnTargetPerceptionUpdated.AddDynamic(this, &AEnemyController::OnTargetPerceptionUpdated);
	}

	//Set Character MovementComponent parameters
	SetOrientRotationToMovement(false);
}

void AEnemyController::OnStimulusHearing_Implementation(AActor* Actor, FAIStimulus Stimulus)
{
}

void AEnemyController::OnStimulusDamage_Implementation(AActor* Actor, FAIStimulus Stimulus)
{
}

void AEnemyController::OnStimulusSight_Implementation(AActor* Actor, FAIStimulus Stimulus)
{
}

void AEnemyController::SetOrientRotationToMovement(bool orientToMovement)
{
	APawn* ControlledPawn = GetPawn();
	if(IsValid(ControlledPawn))
	{
		UCharacterMovementComponent* MoveComp = Cast<UCharacterMovementComponent, UPawnMovementComponent>(
         ControlledPawn->GetMovementComponent());
		if (MoveComp)
		{
			MoveComp->bOrientRotationToMovement=orientToMovement;
			MoveComp->bUseControllerDesiredRotation=!orientToMovement;
		}
	}
}

bool AEnemyController::IsValidTarget(AActor* Actor)
{
	if (Actor)
	{
		auto Component = Cast<UActorStatus>(
			Actor->GetComponentByClass(UActorStatus::StaticClass()));
		if (IsValid(Actor) &&
			IsValid(Component) &&
            Actor->ActorHasTag(TEXT("Player")) &&
            !Component->IsDead()
            )
            	return true;
	}
	return false;
	
}

void AEnemyController::OnTargetPerceptionUpdated_Implementation(AActor* Actor, FAIStimulus Stimulus)
{
		if(SenseConfig_Sight && Stimulus.Type == SenseConfig_Sight->GetSenseID())
		{
			OnStimulusSight(Actor, Stimulus);
		}
	else if(SenseConfig_Damage && Stimulus.Type == SenseConfig_Damage->GetSenseID())
	{
		OnStimulusDamage(Actor, Stimulus);
	}
	else if(SenseConfig_Hearing && Stimulus.Type == SenseConfig_Hearing->GetSenseID())
	{
		OnStimulusHearing(Actor, Stimulus);
	}
}
