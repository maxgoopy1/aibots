// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "EnemyBehavior.generated.h"


/** Enum for identify enemy character behavior
 * Hunting - searching for target, but don't know exact target location
 * Chasing - moving to the target, target location is known
 * Escorting - patrolling near moving target
 * Patrolling - patrolling in the region
 * Hiding - enemy try hiding from target
 * Idle - DEFAULT behavior for enemy, maybe some random movement without reason
 */
UENUM(BlueprintType, Blueprintable)
enum class EEnemyBehavior : uint8
{
	IDLE		= 0 UMETA(DisplayName = "Idle"),
	HIDING		= 1 UMETA(DisplayName = "Hiding"),
	PATROLLING	= 2 UMETA(DisplayName = "Patrolling"),
	ESCORTING	= 3 UMETA(DisplayName = "Escorting"),
    CHASING		= 4 UMETA(DisplayName = "Chasing"),
    HUNTING		= 5 UMETA(DisplayName = "Hunting")
};


/** Enum for identify enemy character behavior
* Melee Attacking - attack target in melee
* Reloading - reload weapon
* Shooting - shooting the target
* Look Around - look around
* Idle - DEFAULT behavior for enemy, maybe some random movement without reason
*/
UENUM(BlueprintType, Blueprintable)
enum class EEnemyBehavior_Action : uint8
{
	IDLE			= 0 UMETA(DisplayName = "Idle"),
    LOOKAROUND		= 1 UMETA(DisplayName = "Look Around"),
    SHOOTING		= 2 UMETA(DisplayName = "Shooting"),
    RELOADING		= 3 UMETA(DisplayName = "Reloading"),
    MELEEATACKING	= 4 UMETA(DisplayName = "Melee Actacking")
};


/*
UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class AIBOTS_API UEnemyBehavior : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UEnemyBehavior();

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

		
};
*/
