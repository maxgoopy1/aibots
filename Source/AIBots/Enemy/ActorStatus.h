// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"

#include "ActorStatus.generated.h"


DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnHealthChangeEvent, class UActorStatus*, Status);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnCharacterDeadEvent, AActor*, Actor);

UENUM(BlueprintType, Blueprintable)
enum class EEnumActorHealthStatus:uint8
{
	FULL_HEALTH		UMETA(DisplayName = "Full health"),
	INJURED			UMETA(DisplayName = "Injured"),
	CRITICAL_HEALTH UMETA(DisplayName = "Critical low health"),
	DYING			UMETA(DisplayName = "Almost dead"),
	DEAD			UMETA(DisplayNAme = "Dead")
};

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class AIBOTS_API UActorStatus : public UActorComponent
{
	friend class UActorStatusState;
	GENERATED_BODY()

	//Owner Health
	UPROPERTY(EditAnywhere)
	float Health;
	
	//Owner health description
	EEnumActorHealthStatus HealthStatus;

public:
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float MaxHealth = 100;
	
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	bool Immortal;
	
	// Sets default values for this component's properties
	UActorStatus();

	//Is HealthStatus equal DEAD
	bool IsDead();
	
	//Event called each time when actor health was changed
	UPROPERTY(BlueprintAssignable)
	FOnHealthChangeEvent OnHealthChangeEvent;

	//Event called on actor dead
	UPROPERTY(BlueprintAssignable)
	FOnCharacterDeadEvent OnCharacterDeadEvent;

	UFUNCTION(BlueprintCallable)
    EEnumActorHealthStatus GetHealthStatus() const { return HealthStatus; }

	UFUNCTION(BlueprintCallable)
    float GetHealth() const {return Health;};

	UFUNCTION(BlueprintCallable)
    float GetMaxHealth() const {return MaxHealth;};

	//Changes health in range of [0..MaxHealth]
	//delta: amount of changing healths
	//return: actual delta
	UFUNCTION(BlueprintCallable)
    float ChangeHealth(float delta);

private:

	UFUNCTION()
	void OnTakeAnyDamage( AActor* DamagedActor, float Damage, const class UDamageType* DamageType, class AController* InstigatedBy, AActor* DamageCauser );
	
protected:
	// Called when the game starts
	virtual void BeginPlay() override;

	UFUNCTION()
	void SetHealthStatus(EEnumActorHealthStatus NewHealthStatus)
	{
		this->HealthStatus = NewHealthStatus;
	}

	UFUNCTION()
	void RefreshHealthStatus();
		
};

