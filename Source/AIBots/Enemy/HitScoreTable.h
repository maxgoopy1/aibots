// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/NoExportTypes.h"
#include "HitScoreTable.generated.h"

/** Class for Contain Score Table
 * key - FString
 * value - float
 * 
 */
UCLASS(MinimalAPI, const, Blueprintable, BlueprintType)
class UHitScoreTable : public UObject
{
	GENERATED_UCLASS_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = HitBox)
	TMap<FString, float> ScoreTable;

	//Default value for Getter 
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = HitBox)
	float DefaultScore;

	//Safe getter of value from table
	UFUNCTION(BlueprintCallable, Category = HitBox)
	float GetScore(const FString& Name) const;
};
