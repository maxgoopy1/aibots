// Fill out your copyright notice in the Description page of Project Settings.


#include "HitScoreTable.h"

UHitScoreTable::UHitScoreTable(const FObjectInitializer& ObjectInitializer) : Super(ObjectInitializer)
{
	ScoreTable.Add(TEXT("Head"), 10);
	DefaultScore = 1;
}

float UHitScoreTable::GetScore(const FString& Name) const
{
	if ( ScoreTable.Num() == 0 ) return DefaultScore;

	auto result  = ScoreTable.Find(Name);
	if (result) return *result;

	return DefaultScore;
}
