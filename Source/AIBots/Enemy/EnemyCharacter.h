// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "AIBots/Character/CharacterBase.h"
#include "ActorStatus.h"
#include "HitScoreTable.h"
#include "Animation/Character3DAnimInstance.h"
#include "Controller/EnemyController.h"
#include "Controller/EnemyWeaponController.h"
#include "Navigation/PatrolArea.h"
#include "GameFramework/Character.h"
#include "AIBots/Items/Weapon/EnemyWeapon.h"


#include "EnemyCharacter.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnMainWeaponChanged, AEnemyWeapon*, NewWeapon);

UCLASS()
class AIBOTS_API AEnemyCharacter : public ACharacterBase
{
	GENERATED_BODY()

	//Should character aim at AimPoint
	UPROPERTY()
	bool Aiming = false;
	
	//Point at which character aim
	UPROPERTY()
	FVector AimPoint;

	//Default point at which character aim in AnimInstance space
	UPROPERTY()
	FVector AimPointDefault = FVector::ZeroVector;

	//Should character track TrackingPoint
	UPROPERTY()
	bool Tracking = false;
	
	//Point at which character look
	UPROPERTY()
	FVector TrackingPoint;

	//Default point at which character look in AnimInstance space
	UPROPERTY()
	FVector TrackingPointDefault = FVector::ZeroVector;

	//Hit score table for damage multiply
	UPROPERTY(EditAnywhere,Category = HitBox)
	TSubclassOf<UHitScoreTable> HitScoreTableClass;

	//Default class spawned for MainWeapon
	UPROPERTY(EditDefaultsOnly)
	TSubclassOf<AEnemyWeapon> DefaultMainWeaponClass;

	//Default class spawned for SecondWeapon
	UPROPERTY(EditDefaultsOnly)
	TSubclassOf<AEnemyWeapon> DefaultSecondWeaponClass;

	//Name of socket in Mesh for attaching active weapon
	UPROPERTY(EditDefaultsOnly)
	FName WeaponActive_SocketName = TEXT("hand_rSocket");

	//Name of socket in Mesh for attaching stacked weapon
	UPROPERTY(EditDefaultsOnly)
	FName WeaponStack_SocketName = TEXT("spineWeaponSocket");
	
public:
	UFUNCTION(BlueprintCallable, BlueprintPure)
	FVector GetTrackingPoint() const { return TrackingPoint;}
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	UActorStatus* ActorStatus;

	/*Time after which character wil be destroyed after pending DEATH
	*If <0 then character will not be destroyed
	*/
	UPROPERTY(EditDefaultsOnly)
	float DestructTime = 0;

	//MainWeapon
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	AEnemyWeapon* MainWeapon;

	//SecondWeapon
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	AEnemyWeapon* SecondWeapon;

	//WeaponController
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	UEnemyWeaponController* WeaponController;

public:
	// Sets default values for this character's properties
	AEnemyCharacter(const FObjectInitializer& ObjectInitializer);

	virtual void EndPlay(const EEndPlayReason::Type EndPlayReason) override;

	//Is Character DEAD
	UFUNCTION(BlueprintCallable)
	bool IsDead();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	//Called before destroying the object.
	virtual void BeginDestroy() override;
	
	//Called when character dead
	UFUNCTION(BlueprintNativeEvent, BlueprintCallable)
	void OnCharacterDead(AActor* Actor);

	float InternalTakePointDamage(float Damage, FPointDamageEvent const& PointDamageEvent, AController* EventInstigator, AActor* DamageCauser) override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	/** Attach Patrol Area to existing set of Patrol Areas
	 * @param Area Attaching Patrol Area
	 */
	UFUNCTION(BlueprintImplementableEvent)
	void AttachPatrolArea(APatrolArea* Area);
		
	/** Command the character to aim at the point
	 * @param NewAimPoint Point in world space
	 * @return: true if can aim to the point
	 */
	UFUNCTION(BlueprintCallable)
	bool AimToPoint(FVector NewAimPoint = FVector::ZeroVector);

	/**Command the character to rest aim point
	 * Aim point will be set to default
	 */
	UFUNCTION(BlueprintCallable)
	void AimPointReset();

	/** Command the character to view at the point
	 * @param NewTrackingPoint Point in world space
	 * @return: true if view to the point
	 */
	UFUNCTION(BlueprintCallable)
    bool TrackPoint(FVector NewTrackingPoint = FVector::ZeroVector);

	/**Command the character to rest tracking point
	* Tracking point will be set to default
	*/
	UFUNCTION(BlueprintCallable)
    void TrackingPointReset();

	UFUNCTION(BlueprintNativeEvent, BlueprintCallable)
	void Shoot();
	
	UFUNCTION(BlueprintCallable)
	bool ShootThePoint(FVector Point);
	
	//Event called on actor dead
	UPROPERTY(BlueprintAssignable)
	FOnCharacterDeadEvent OnCharacterDeadEvent;

	//Do default MeleeAttack
	UFUNCTION(BlueprintNativeEvent,BlueprintCallable)
	void DoMeleeAttack();

	//Event from Anim instance about finished MeleeAtack
	UPROPERTY(BlueprintAssignable)
	FOnMeleeAttackFinished OnMeleeAttackFinished;

	/** Set new MainWeapon from Class or existing AEnemyWeapon
	 * @param NewWeapon - Existing weapon that mast be set as Main, if invalid then EnemyWeaponClass will be used
	 * @param EnemyWeaponClass - Class of new MainWeaopon, if undefined then DefaultMainWeaponClass will be used
	 * @param DestroyOldWeapon - if true then CurrentWeapon will be Destroyed, else will be dropped
	 * @return Pointer to new MainWeapon as AEnemyWeapon
	 */
	UFUNCTION(BlueprintCallable)
    AEnemyWeapon* SetMainWeapon(AEnemyWeapon* NewWeapon = nullptr ,TSubclassOf<AEnemyWeapon> EnemyWeaponClass = nullptr, bool DestroyOldWeapon = true);

	/** Set new SecondWeapon from Class or existing AEnemyWeapon
	 *@param NewWeapon - Existing weapon that mast be set as Second, if invalid then EnemyWeaponClass will be used
	 * @param EnemyWeaponClass - Class of new SecondWeaopon, if undefined then DefaultSecondWeaponClass will be used
	 * @param DestroyOldWeapon - if true then CurrentWeapon will be Destroyed, else will be dropped
	 * @return Pointer to new SecondWeapon as AEnemyWeapon
	 */
	UFUNCTION(BlueprintCallable)
    AEnemyWeapon* SetSecondWeapon(AEnemyWeapon* NewWeapon = nullptr, TSubclassOf<AEnemyWeapon> EnemyWeaponClass = nullptr, bool DestroyOldWeapon = true);


	/**Detach MainWeapon from character
	 * @param DestroyWeapon - Destroy weapon after Detaching
	 * @return Return pointer to dropped weapon as AEnemyWeapon 
	 */
	UFUNCTION(BlueprintCallable)
	AEnemyWeapon* DropMainWeapon(bool DestroyWeapon = true);

	
	/**Detach SecondWeapon from character
	* @param DestroyWeapon - Destroy weapon after Detaching
	* @return Return pointer to dropped weapon as AEnemyWeapon 
	*/
	UFUNCTION(BlueprintCallable)
    AEnemyWeapon* DropSecondWeapon(bool DestroyWeapon = true);
	
	UFUNCTION(BlueprintCallable)
	AEnemyWeapon* InitializeDefaultMainWeapon();

	UFUNCTION(BlueprintCallable)
	AEnemyWeapon* InitializeDefaultSecondWeapon();

	/**Switch Main and Second weapon
	 */
	UFUNCTION(BlueprintCallable)
	void SwitchWeapon();

	UPROPERTY(BlueprintAssignable)
	FOnMainWeaponChanged OnMainWeaponChanged;
private:
	UFUNCTION()
	void OnHealthChange(UActorStatus* Status);

	//Get Muzzle of the weapon in world space 
	UFUNCTION()
	bool GetMuzzleLocation(FVector& MuzzleLocation);

	UFUNCTION()
	void OnCharacterMovementUpdatedFunction( float DeltaSeconds, FVector OldLocation, FVector OldVelocity);

	/** Calculate relative rotator end send them to the AnimInstance
	 * @param NewTrackingPoint - Point in world Space
	 */
	bool TrackPointInner(FVector NewTrackingPoint);
	
	/** Calculate relative rotator end send them to the AnimInstance
	* @param NewAimPoint - Point in world Space
	*/
	bool AimToPointInner(FVector NewAimPoint);

	/** Calculate relative rotator end send them to the AnimInstance
	* @param NewTrackingPoint - Point in AnimInstance Space
	*/
	bool TrackPointRelative(FVector NewTrackingPoint);

	/** Calculate relative rotator end send them to the AnimInstance
	* @param NewAimPoint - Point in AnimInstance Space
	*/
	bool AimToPointRelative(FVector NewAimPoint);

	UFUNCTION()
	void OnMeleeAttackFinished_Inner();

	//Actions required before destroying
	UFUNCTION()
	void InnerDestroy();

};
