// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/NoExportTypes.h"
#include "DamageTrace.generated.h"

/**
 * 
 */
UCLASS()
class AIBOTS_API UDamageTrace : public UObject
{
	GENERATED_BODY()

	/**Trace line from Observer to TargetActor
	* @param TargetActor Actor wich will be traced
	* @param ObserverLocation Trace from location
	* @param OutHitLocation Output Hit location
	* @param OutHitResult Output Hit result
	* @param NumberOfLoSChecksPerformed Parameters increased on each Trace
	* @param IgnoreActor Actor that will be ignored in trace
	* @param TraceChanel Chanel for trace
	* @param MultiTrace Use MultiTrace in other case SingleTrace
	* @return If trace from Observer to TargetActor can be succeed
	*/
	UFUNCTION(BlueprintCallable)
    static bool ActorLineTraceByChanel(AActor* TargetActor, const FVector& ObserverLocation, FVector& OutHitLocation, FHitResult& OutHitResult, int32& NumberOfLoSChecksPerformed, const AActor* IgnoreActor, ECollisionChannel TraceChanel = ECollisionChannel::ECC_MAX, bool MultiTrace = false);

};
