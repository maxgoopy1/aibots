// Fill out your copyright notice in the Description page of Project Settings.


#include "DamageTrace.h"

bool UDamageTrace::ActorLineTraceByChanel(AActor* TargetActor, const FVector& ObserverLocation, FVector& OutHitLocation, FHitResult& OutHitResult, int32& NumberOfLoSChecksPerformed,
	const AActor* IgnoreActor, ECollisionChannel TraceChanel, bool MultiTrace)
{
	if (!IsValid(TargetActor))
		return false;
	
	const UWorld* World = TargetActor->GetWorld();
	if (!World)
		return false;

	bool bHit = false;
	
	//If TraceChanel not set then use Visibility chanel
	if (TraceChanel == ECollisionChannel::ECC_MAX)
		TraceChanel = ECC_Visibility;
	
	if(!MultiTrace)
	{
		//Single trace
		
		bHit = World->LineTraceSingleByChannel(OutHitResult,
                  ObserverLocation,
                  TargetActor->GetActorLocation(),
                  TraceChanel,
                  FCollisionQueryParams(SCENE_QUERY_STAT(AILineOfSight),true, IgnoreActor));
				
		++NumberOfLoSChecksPerformed;

		//If successfully hit TargetActor return its location
		if(bHit && OutHitResult.Actor == TargetActor)
		{
			OutHitLocation = TargetActor->GetActorLocation();
			return true;
		}
	}
	else
	{
		//Multi trace
		
		TArray<FHitResult> Hits;
		bHit = World->LineTraceMultiByChannel(Hits,
            ObserverLocation,
            TargetActor->GetActorLocation(),
            TraceChanel,
            FCollisionQueryParams(TEXT("DamageTrace"),true,IgnoreActor));
		
		FHitResult* Hit = Hits.FindByPredicate([TargetActor](FHitResult Hit)
            {
                return Hit.Actor == TargetActor;
            });
    				
		NumberOfLoSChecksPerformed+=Hits.Num();
    				
		//If successfully hit TargetActor return its location
		if(Hit != nullptr)
		{
			OutHitResult = *Hit;
			OutHitLocation = TargetActor->GetActorLocation();
			return true;
		}
	}

	return false;
}