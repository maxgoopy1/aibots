// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/DamageType.h"
#include "DamageType_Bullet.generated.h"

/**
 * 
 */
UCLASS()
class AIBOTS_API UDamageType_Bullet : public UDamageType
{
	GENERATED_BODY()

	UDamageType_Bullet(const FObjectInitializer& ObjectInitializer);
};
