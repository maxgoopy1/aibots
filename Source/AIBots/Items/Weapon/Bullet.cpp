// Fill out your copyright notice in the Description page of Project Settings.


#include "Bullet.h"

#include "DamageType_Bullet.h"
#include "Kismet/GameplayStatics.h"
#include "Perception/AISense_Damage.h"

// Sets default values
ABullet::ABullet()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
}

// Called when the game starts or when spawned
void ABullet::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ABullet::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void ABullet::OnHit(AActor *other, FHitResult HitResult)
{
	AActor* DamagedActor = HitResult.GetActor();
	
	if (!IsValid(DamagedActor))
	{
		Destroy();
		return;
	}

	if (GetOwner() == DamagedActor)
		return;
	
	//Calculate DamageInstigator location
	FVector EventLocation;
	if (IsValid(DamageInstigator))
	{
		EventLocation = DamageInstigator->GetActorLocation();
	}
	else
	{
		EventLocation = GetVelocity();
		EventLocation.Normalize();
		EventLocation*=-500;
		EventLocation += DamagedActor->GetActorLocation();
	};
	
	UGameplayStatics::ApplyPointDamage(DamagedActor,
        Damage,
        EventLocation,
        HitResult,
        DamageInstigator ? DamageInstigator->GetInstigatorController() : nullptr,
        this,
        UDamageType_Bullet::StaticClass());

	//Inform the Perception System about Hit
	UAISense_Damage::ReportDamageEvent(GetWorld(),
		DamagedActor,
		DamageInstigator,
		Damage,
		EventLocation,
		HitResult.Location);
	
	Destroy();
}
