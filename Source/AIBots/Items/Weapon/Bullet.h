// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Bullet.generated.h"

UCLASS()
class AIBOTS_API ABullet : public AActor
{
	GENERATED_BODY()
	
	//Damage causer
	UPROPERTY()
	AActor* DamageInstigator;

public:
	UFUNCTION(BlueprintCallable)
	AActor* GetDamageInstigator() const { return DamageInstigator; }
	
	UFUNCTION(BlueprintCallable)
	void SetDamageInstigator(AActor* NewDamageInstigator){	DamageInstigator = NewDamageInstigator; }

	// Sets default values for this actor's properties
	ABullet();
	
protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	UFUNCTION(BlueprintCallable)
	void OnHit(AActor *other, FHitResult HitResult);

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float Damage;
};
