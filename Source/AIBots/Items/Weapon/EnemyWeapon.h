// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "EnemyWeapon.generated.h"


UENUM(BlueprintType, Blueprintable)
enum class EWeaponType : uint8
{
	MELEE_WEAPON = 0 UMETA(DisplayName = "Melee weapon"),
	FIREARMS = 1 UMETA(DisplayName = "Firearms"),
	THROWING_WEAPON = 2 UMETA(DisplayName = "Throwing weapon")
	
};


UENUM(BlueprintType, Blueprintable)
enum class EWeaponCategory : uint8
{
	UNDEFINE	= 0 UMETA(DisplayName = "UNDEFINE for developing only"),
	BAT			= 1 UMETA(DisplayName = "Bat"),
    AXE			= 2 UMETA(DisplayName = "Axe"),
	RIFLE		= 3 UMETA(DisplayName = "Rifle"),
    PISTOL		= 4 UMETA(DisplayName = "Pistol")
};

/**
 * 
 */
UCLASS()
class AIBOTS_API AEnemyWeapon : public AActor
{
	GENERATED_BODY()
	
	//Weapon Type
	UPROPERTY(EditDefaultsOnly, Category="Weapon")
	EWeaponType WeaponType = EWeaponType::MELEE_WEAPON;

	//Weapon Category of selected type
	UPROPERTY(EditDefaultsOnly, Category="Weapon")
	EWeaponCategory WeaponCategory = EWeaponCategory::BAT;

	UPROPERTY(EditAnywhere, Category="Weapon")
	float AttackRange = 1500;

public:
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
	USkeletalMeshComponent* Mesh;

public:
	AEnemyWeapon();
	
	virtual void BeginPlay() override;
	
	UFUNCTION(BlueprintCallable)
	EWeaponType GetWeaponType() const { return WeaponType;}

	UFUNCTION(BlueprintCallable)
	void SetWeaponType(EWeaponType NewWeaponType){	this->WeaponType = WeaponType;}

	UFUNCTION(BlueprintCallable)
	EWeaponCategory GetWeaponCategory() const {	return WeaponCategory;}

	UFUNCTION(BlueprintCallable)
	void SetWeaponCategory(EWeaponCategory NewWeaponCategory) {		this->WeaponCategory = WeaponCategory;	}

	UFUNCTION(BlueprintCallable)
    float GetAttackRange() const
	{
		return AttackRange;
	}

	UFUNCTION(BlueprintCallable)
    void SetAttackRange(float NewAttackRange)
	{
		this->AttackRange = NewAttackRange;
	}

	//Set params for collision and physics which are required to exists on the level
	UFUNCTION(BlueprintNativeEvent, BlueprintCallable)
    void OnDrop();

	//Set params for collision and physics which required to be attached to Character
	UFUNCTION(BlueprintNativeEvent, BlueprintCallable)
    void OnPickup();

	//World Space muzzle location
	UFUNCTION(BlueprintCallable, BlueprintImplementableEvent, BlueprintPure)
	FVector GetMuzzleLocation();
};
