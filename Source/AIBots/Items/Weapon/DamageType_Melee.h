// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/DamageType.h"
#include "DamageType_Melee.generated.h"

/**
 * 
 */
UCLASS()
class AIBOTS_API UDamageType_Melee : public UDamageType
{
	GENERATED_BODY()

	UDamageType_Melee(const FObjectInitializer& ObjectInitializer);
};
