// Fill out your copyright notice in the Description page of Project Settings.


#include "EnemyWeapon.h"

AEnemyWeapon::AEnemyWeapon()
{
}

void AEnemyWeapon::BeginPlay()
{
	Super::BeginPlay();
	if(!Mesh)
	{
		Mesh = Cast<USkeletalMeshComponent>(GetComponentByClass(USkeletalMeshComponent::StaticClass()));
	}
}

void AEnemyWeapon::OnPickup_Implementation()
{
	UE_LOG(LogTemp, Warning, TEXT("No implementation of OnPickup"));
}

void AEnemyWeapon::OnDrop_Implementation()
{
	UE_LOG(LogTemp, Warning, TEXT("No implementation of OnDrop"));
}