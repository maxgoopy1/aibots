// Fill out your copyright notice in the Description page of Project Settings.


#include "CharacterBase.h"

// Sets default values
ACharacterBase::ACharacterBase(const FObjectInitializer& ObjectInitializer) :
Super(ObjectInitializer)
{
 	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void ACharacterBase::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ACharacterBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

// Called to bind functionality to input
void ACharacterBase::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

}

bool ACharacterBase::CanBeSeenFrom(const FVector& ObserverLocation, FVector& OutSeenLocation,
	int32& NumberOfLoSChecksPerformed, float& OutSightStrength, const AActor* IgnoreActor) const
{
	//Not implemented, always 1
	OutSightStrength = 1;
	FHitResult Hit;
	return CanBeDamagedFrom(ObserverLocation, OutSeenLocation,Hit, NumberOfLoSChecksPerformed, IgnoreActor);
}

bool ACharacterBase::CanBeDamagedFrom(const FVector& ObserverLocation, FVector& OutHitLocation, FHitResult& OutHitResult, int32& NumberOfLoSChecksPerformed,
	const AActor* IgnoreActor, ECollisionChannel TraceChanel, bool MultiTrace) const
{
	const UWorld* World = GetWorld();
	if (!World)
		return false;

	//If we have valid BoneNameTable then check trace from ObserverLocation to one of the Bone
	auto Names = &BoneNameTableClass.GetDefaultObject()->Names;
	bool bHit = false;
	//If TraceChanel not set then use trace chanel from BoneNameTableClass
	if (TraceChanel == ECollisionChannel::ECC_MAX)
		TraceChanel = BoneNameTableClass.GetDefaultObject()->TraceChanel.GetValue();
    if(Names->Num()!=0 && GetMesh()!=nullptr)
    {
    	FVector BoneLocation;

    	//Go through all Bones in table
    	for (auto Name : *Names)
    	{
    		//If mesh has Bone with that Name do trace
    		if(GetMesh()->GetBoneIndex(Name) != INDEX_NONE)
    		{
				//Single trace
    			
    			BoneLocation = GetMesh()->GetBoneLocation(Name);
    			if(!MultiTrace)
    			{
    				bHit = World->LineTraceSingleByChannel(OutHitResult,
                  ObserverLocation,
                  BoneLocation,
                  TraceChanel,
                  FCollisionQueryParams(SCENE_QUERY_STAT(AILineOfSight),true, IgnoreActor));
				
    				++NumberOfLoSChecksPerformed;

    				//If successfully hit self return bone location
    				if(bHit && OutHitResult.Actor == this)
    				{
    					OutHitLocation = BoneLocation;
    					return true;
    				}
    			}
    			else
    			{
    				//Multi trace
    				
    				TArray<FHitResult> Hits;
    				bHit = World->LineTraceMultiByChannel(Hits,
    					ObserverLocation,
    					BoneLocation,
    					TraceChanel,
    					FCollisionQueryParams(TEXT("DamageTrace"),true,IgnoreActor));
    				FHitResult* Hit = Hits.FindByPredicate([this](FHitResult Hit)
                        {
                            return Hit.Actor == this;
                        });

    				NumberOfLoSChecksPerformed+=Hits.Num();
    				
    				//If successfully hit self return bone location
    				if(Hit != nullptr)
    				{
    					OutHitResult = *Hit;
    					OutHitLocation = BoneLocation;
    					return true;
    				}
    			}
    		}
    	}
    }

	//Other case check trace to this Location

	if(!MultiTrace)
	{
		//Single trace
		
		bHit = World->LineTraceSingleByChannel(OutHitResult,
                  ObserverLocation,
                  GetActorLocation(),
                  TraceChanel,
                  FCollisionQueryParams(SCENE_QUERY_STAT(AILineOfSight),true, IgnoreActor));
				
		++NumberOfLoSChecksPerformed;

		//If successfully hit self return this location
		if(bHit && OutHitResult.Actor == this)
		{
			OutHitLocation = GetActorLocation();
			return true;
		}
	}
	else
	{
		//Multi trace
		
		TArray<FHitResult> Hits;
		bHit = World->LineTraceMultiByChannel(Hits,
            ObserverLocation,
            GetActorLocation(),
            TraceChanel,
            FCollisionQueryParams(TEXT("DamageTrace"),true,IgnoreActor));
		FHitResult* Hit = Hits.FindByPredicate([this](FHitResult Hit)
            {
                return Hit.Actor == this;
            });
    				
		NumberOfLoSChecksPerformed+=Hits.Num();
    				
		//If successfully hit self return this location
		if(Hit != nullptr)
		{
			OutHitResult = *Hit;
			OutHitLocation = GetActorLocation();
			return true;
		}
	}

	return false;
}

