// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/NoExportTypes.h"
#include "SightTraceBoneNameTable.generated.h"

/**
 * 
 */
UCLASS(MinimalAPI, const, Blueprintable, BlueprintType)
class USightTraceBoneNameTable : public UObject
{
	GENERATED_UCLASS_BODY()

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category=HitBox)
	TArray<FName> Names;

	UPROPERTY(EditDefaultsOnly)
	TEnumAsByte<ECollisionChannel> TraceChanel;
	
};
