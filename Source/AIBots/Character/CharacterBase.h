// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"

#include "SightTraceBoneNameTable.h"
#include "GameFramework/Character.h"
#include "Perception/AISightTargetInterface.h"

#include "CharacterBase.generated.h"

UCLASS(Abstract)
class AIBOTS_API ACharacterBase : public ACharacter, public IAISightTargetInterface
{
	GENERATED_BODY()

	UPROPERTY(EditDefaultsOnly, Category=HitBox)
	TSubclassOf<USightTraceBoneNameTable> BoneNameTableClass;

public:
	// Sets default values for this character's properties
	ACharacterBase(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get());

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	//AISight Interface
	UFUNCTION(BlueprintCallable)
	virtual bool CanBeSeenFrom(const FVector& ObserverLocation, FVector& OutSeenLocation, int32& NumberOfLoSChecksPerformed, float& OutSightStrength, const AActor* IgnoreActor) const override;

	/**Trace line from Observer to this actor using BoneNameTableClass
	 * @param ObserverLocation Trace from location
	 * @param OutHitLocation Output HitLocation
	 * @param OutHitResult Successful hit result
	 * @param NumberOfLoSChecksPerformed Parameters increased on each Trace
	 * @param IgnoreActor Actor that will be ignored in trace
	 * @param TraceChanel Chanel for trace
	 * @param MultiTrace Use MultiTrace in other case SingleTrace
	 * @return If trace from Observer to this actor can be succeed
	 */
	UFUNCTION(BlueprintCallable)
    virtual bool CanBeDamagedFrom(const FVector& ObserverLocation, FVector& OutHitLocation, FHitResult& OutHitResult, int32& NumberOfLoSChecksPerformed, const AActor* IgnoreActor, ECollisionChannel TraceChanel = ECollisionChannel::ECC_MAX, bool MultiTrace = false) const;

};


